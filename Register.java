package stock_exchange_2;
import java.util.*;
public class Register {
	ArrayList<Customer> registered_customers = new ArrayList<Customer>();
	ArrayList<Stock> registered_stocks = new ArrayList<Stock>();
	
	public void register_new_customer(String name, float cred)
	{
		Customer new_cust = new Customer(name,cred);
		registered_customers.add(new_cust);
	}
	
	public void register_new_stock(String name,float price)
	{
		Stock new_stock = new Stock(name,price);
		registered_stocks.add(new_stock);
	}
	
	/*The following function allows us to check if the given stock is registered or not
	 *the function takes name of stock as input and returns -1 if no such stock has been
	 *registered with the application. it will return the index of the stock in the list
	 *if the stock was found in the registered list of stocks*/
	public int check_stock(String name)
	{
		int pos = -1;
		for(int i=0;i<registered_stocks.size();i++)
		{
			if(registered_stocks.get(i).name.equals(name))
			{
				pos = i;
				break;
			}
		}
		return pos;
	}
	
	/*The following function allows us to check if the given customer is registered or not
	 *the function takes name of a customer as input and returns -1 if no such customer has been
	 *registered with the application. it will return the index of the customer in the list
	 *if the customer was found in the registered list of customer*/
	public int check_customer(String name)
	{
		int pos = -1;
		for(int i=0;i<registered_customers.size();i++)
		{
			if(registered_customers.get(i).name.equals(name))
			{
				pos = i;
				break;
			}
		}
		return pos;
	}
	
	public void print_customers()
	{
		System.out.println("Name\tOutsta\tcredit");
		for(int i=0;i<registered_customers.size();i++)
		{
			Customer curr = registered_customers.get(i);
			System.out.println(curr.name + "\t" + curr.outstanding + "\t" + curr.credit);
		}
	}
	
}
