package stock_exchange_2;

public class Trade {
	float price; // the seller price
	int time;
	String stock; // The name of stock
	String buyer; // Buyer name
	float quantity; // the quantity the seller can sell
	String seller; // Seller Name
	public void setPrice(float price) {
		this.price = price;
	}
	
	public void setStock(String stock) {
		this.stock = stock;
	}
	
	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}
	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}
	public void setSeller(String seller) {
		this.seller = seller;
	}
	public float getPrice() {
		return price;
	}
	
	public String getStock() {
		return stock;
	}
	
	public float getQuantity() {
		return quantity;
	}
	
	
	public String getBuyer() {
		return buyer;
	}
	
	public String getSeller() {
		return seller;
	}
}
