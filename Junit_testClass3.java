package stock_exchange_2;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Test;

public class Junit_testClass3 {

	//@SuppressWarnings("deprecation")
	//@SuppressWarnings("deprecation")
	@Test
	public void test() {
		
		
		//Case 3: Order matches but customers outstanding exceeds credit
		Register reg = new Register();
		reg.register_new_stock("S1",30);
		reg.register_new_stock("S2",50);
		
		reg.register_new_customer("C1", 50);
		reg.register_new_customer("C2", 20);
		reg.register_new_customer("C3", 200);
		
		Order[] o = new Order[3];
		o[0] = new Order();
		o[1] = new Order();
		o[2] = new Order();

		
		o[0].setTime(1);
		o[0].setType("S");
		o[0].setClient("C3");
		o[0].setStock("S1");
		o[0].setQuantity(10);
		o[0].setPrice(30);
		
		
		//this order should only partially match as 2 stocks would exceed credit for C1
		o[1].setTime(2);  
		o[1].setType("B");
		o[1].setClient("C1");
		o[1].setStock("S1");
		o[1].setQuantity(5);
		o[1].setPrice(30);
		
		
		// this order should’nt match with any and must remain pending
		o[2].setTime(3);
		o[2].setType("B");
		o[2].setClient("C2");
		o[2].setStock("S1");
		o[2].setQuantity(2);
		o[2].setPrice(30);
		
		
		ArrayList <Order> order_list=new ArrayList <Order>();
		Collections.addAll(order_list, o[0], o[1], o[2]);
		
		Trade [] trd = new Trade[1];
		trd[0] = new Trade();
		
		trd[0].setBuyer("C1");
		trd[0].setPrice(30);
		trd[0].setQuantity(1);
		trd[0].setSeller("C3");
		trd[0].setStock("S1");
		trd[0].time = 2;

		
		
		ArrayList <Trade> trade_list=new ArrayList <Trade>();
		Collections.addAll(trade_list, trd[0]);
		
		pending_orders pend = new pending_orders();
		
		Order_match match = new Order_match();
		
		for(int i=0;i<5;i++)
		{
			pend.add_new_order(order_list.get(i));
			pend.update_orders(match.findMatch(pend.orders,reg));
		}
		ArrayList <Trade> trade_list_byProgram = match.trades;
		for(int i=0;i<trade_list.size();i++) {
			// checking all the attribute values of both lists
			assertEquals(trade_list.get(i).time,trade_list_byProgram.get(i).time);
			assertEquals(trade_list.get(i).getBuyer(),trade_list_byProgram.get(i).getBuyer());
			assertEquals(trade_list.get(i).getQuantity(),trade_list_byProgram.get(i).getQuantity(), 0.01);
			assertEquals(trade_list.get(i).getPrice(),trade_list_byProgram.get(i).getPrice(), 0.01);
			assertEquals(trade_list.get(i).getStock(),trade_list_byProgram.get(i).getStock());
			assertEquals(trade_list.get(i).getSeller(),trade_list_byProgram.get(i).getSeller());
			}
	}

}
