package stock_exchange_2;

import java.util.*;

public class Order_match {
	ArrayList<Trade> trades = new ArrayList<Trade>(); // This holds all the trades made which can be used for future use
	
	/*The findmatch function returns the new updated order list after processing all the data.*/
	public ArrayList<Order> findMatch(ArrayList<Order> order , Register data) {	
		for (int i=0; i< order.size();i++) {
			for (int j=i+1;j<order.size();j++) {
				// take an order and match with the remaining orders
				Order o1 = order.get(i);
				Order o2 = order.get(j);
				Trade trade = new Trade();
				
				if (o1.getType().contentEquals("S") && o2.getType().contentEquals("B")) {
					if (o2.getStock().contentEquals(o1.getStock())) {
						if (o2.getPrice() >= o1.getPrice()) {
							
							//This is to allow only a certain quantity so as to not exceed the credit limit of the customer
							int index_buying_cust=data.check_customer(o2.client);
							float cust_credit = data.registered_customers.get(index_buying_cust).credit;
							float cust_outstanding = data.registered_customers.get(index_buying_cust).outstanding;
							float allowance = cust_credit - cust_outstanding; // Available credit for purchase
							float possible_quantity = allowance/o1.price; //Quantity of the stock the customer can buy
							int actual_quantity = (int) possible_quantity; //Lower integer as we dont allow partial buying of stock
							if(actual_quantity > o2.quantity)
							{
								actual_quantity = (int) o2.quantity;
							}
							
							
							if(actual_quantity>0)
							{
								trade.setPrice(o1.getPrice());
								trade.time=o2.getTime();
								trade.setBuyer(o2.getClient());
								trade.setSeller(o1.getClient());
								trade.setStock(o2.getStock());
								if(actual_quantity>=o1.getQuantity())
								{
									trade.setQuantity(o1.getQuantity());
									trades.add(trade);
									o2.setQuantity(o2.getQuantity()-o1.getQuantity());
									o1.setQuantity(0);
									order.set(i, o1);
									order.set(j, o2);
								}
								else
								{
									trade.setQuantity(actual_quantity);
									trades.add(trade);
									o1.setQuantity(o1.getQuantity()-actual_quantity);
									o2.setQuantity(o2.quantity-actual_quantity);
									order.set(i, o1);
									order.set(j, o2);
								}
								
								//The following part is to update the outstanding of the customers
								int index_selling_cust = data.check_customer(trade.seller);
								Customer temp = data.registered_customers.get(index_selling_cust);
								temp.outstanding -= trade.quantity*trade.price;
								data.registered_customers.set(index_selling_cust, temp);
								temp = data.registered_customers.get(index_buying_cust);
								temp.outstanding += trade.quantity*trade.price;
								data.registered_customers.set(index_buying_cust, temp);
							}
						}
					}
				}
				
				if (o1.getType().contentEquals("B") && o2.getType().contentEquals("S")) {
					if (o2.getStock().contentEquals(o1.getStock())) {
						if (o1.getPrice() >= o2.getPrice()) {
							
							//This is to allow only a certain quantity so as to not exceed the credit limit of the customer
							int index_buying_cust=data.check_customer(o1.client);
							float cust_credit = data.registered_customers.get(index_buying_cust).credit;
							float cust_outstanding = data.registered_customers.get(index_buying_cust).outstanding;
							float allowance = cust_credit - cust_outstanding;
							float possible_quantity = allowance/o2.price;
							int actual_quantity = (int) possible_quantity;
							if(actual_quantity > o1.quantity)
							{
								actual_quantity = (int) o1.quantity;
							}
							
							
							if(actual_quantity>0)
							{
								trade.setPrice(o2.getPrice());
								trade.time=o2.getTime();
								trade.setBuyer(o1.getClient());
								trade.setSeller(o2.getClient());
								trade.setStock(o1.getStock());
								if(actual_quantity>=o2.getQuantity())
								{
									trade.setQuantity(o2.getQuantity());
									trades.add(trade);
									o1.setQuantity(o1.getQuantity()-o2.getQuantity());
									o2.setQuantity(0);
									order.set(i, o1);
									order.set(j, o2);
								}
								else
								{
									trade.setQuantity(actual_quantity);
									trades.add(trade);
									o2.setQuantity(o2.getQuantity()-actual_quantity);
									o1.setQuantity(o1.quantity-actual_quantity);
									order.set(i, o1);
									order.set(j, o2);
								}
								
								//The following part is to update the outstanding of the customers
								int index_selling_cust = data.check_customer(trade.seller);
								Customer temp = data.registered_customers.get(index_selling_cust);
								temp.outstanding -= trade.quantity*trade.price;
								data.registered_customers.set(index_selling_cust, temp);
								temp = data.registered_customers.get(index_buying_cust);
								temp.outstanding += trade.quantity*trade.price;
								data.registered_customers.set(index_buying_cust, temp);
							}
						}
					}
				}
			}
		}
		return order;
	}
	
	public void print_customerwise(String name)
	{
		System.out.println("The transactions for customer: "+ name+ "\ntime\tBuyer\tSeller\tStock\tQnt\tPrice");
		for(int i=0 ; i<trades.size();i++)
		{
			Trade temp = trades.get(i);
			if(temp.buyer.equals(name) || temp.seller.equals(name))
			{
				System.out.println(temp.time +"\t" +temp.buyer
						+"\t" + temp.seller + "\t" + temp.stock
						+"\t" + temp.quantity + "\t" + temp.price);
			}
		}
		
	}
	
	public void print_trades()
	{
		System.out.println("time\tBuyer\tSeller\tStock\tQnt\tPrice");
		for(int i=0 ; i<trades.size();i++)
		{
			Trade temp = trades.get(i);
			System.out.println(temp.time +"\t" +temp.buyer
					+"\t" + temp.seller + "\t" + temp.stock
					+"\t" + temp.quantity + "\t" + temp.price);
		}
	}
}
