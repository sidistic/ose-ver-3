package stock_exchange_2;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Test;

public class Junit_testClass2 {

	//@SuppressWarnings("deprecation")
	//@SuppressWarnings("deprecation")
	@Test
	public void test() {
		
		
		//Case 2: Price of stock > 10 % or less than 10%
		Register reg = new Register();
		reg.register_new_stock("pen",20);
		reg.register_new_stock("copy",60);
		
		reg.register_new_customer("B1", 1000);
		reg.register_new_customer("S1", 1000);
		reg.register_new_customer("S2", 1000);
		reg.register_new_customer("B2", 500);
		
		Order[] o = new Order[5];
		o[0] = new Order();
		o[1] = new Order();
		o[2] = new Order();
		o[3] = new Order();
		o[4] = new Order();

		
		o[0].setTime(1);
		o[0].setType("S");
		o[0].setClient("S1");
		o[0].setStock("pen");
		o[0].setQuantity(100);
		o[0].setPrice(30); //price is greater than 10%
		
		o[1].setTime(2);
		o[1].setType("S");
		o[1].setClient("S2");
		o[1].setStock("ball");
		o[1].setQuantity(200);
		o[1].setPrice(19);
		
		o[2].setTime(3);
		o[2].setType("B");
		o[2].setClient("B1");
		o[2].setStock("pen");
		o[2].setQuantity(10);
		o[2].setPrice(20);
		
		o[3].setTime(4);
		o[3].setType("B");
		o[3].setClient("B2");
		o[3].setStock("pen");
		o[3].setQuantity(1000);
		o[3].setPrice(19);
		
		o[4].setTime(5);
		o[4].setType("S");
		o[4].setClient("S2");
		o[4].setStock("pen");
		o[4].setQuantity(2);
		o[4].setPrice(5); //price is less than 10%
		
		ArrayList <Order> order_list=new ArrayList <Order>();
		Collections.addAll(order_list, o[0], o[1], o[2], o[3], o[4]);
		
		Trade [] trd = new Trade[2];
		trd[0] = new Trade();
		trd[1] = new Trade();
		
		trd[0].setBuyer("B1");
		trd[0].setPrice(19);
		trd[0].setQuantity(10);
		trd[0].setSeller("S2");
		trd[0].setStock("pen");
		trd[0].time = 3;

		trd[1].setBuyer("B2");
		trd[1].setPrice(19);
		trd[1].setQuantity(26);
		trd[1].setSeller("S2");
		trd[1].setStock("pen");
		trd[1].time = 4;
		
		
		ArrayList <Trade> trade_list=new ArrayList <Trade>();
		Collections.addAll(trade_list, trd[0], trd[1]);
		
		pending_orders pend = new pending_orders();
		
		Order_match match = new Order_match();
		
		for(int i=0;i<5;i++)
		{
			pend.add_new_order(order_list.get(i));
			pend.update_orders(match.findMatch(pend.orders,reg));
		}
		ArrayList <Trade> trade_list_byProgram = match.trades;
		for(int i=0;i<trade_list.size();i++) {
			// checking all the attribute values of both lists
			assertEquals(trade_list.get(i).time,trade_list_byProgram.get(i).time);
			assertEquals(trade_list.get(i).getBuyer(),trade_list_byProgram.get(i).getBuyer());
			assertEquals(trade_list.get(i).getQuantity(),trade_list_byProgram.get(i).getQuantity(), 0.01);
			assertEquals(trade_list.get(i).getPrice(),trade_list_byProgram.get(i).getPrice(), 0.01);
			assertEquals(trade_list.get(i).getStock(),trade_list_byProgram.get(i).getStock());
			assertEquals(trade_list.get(i).getSeller(),trade_list_byProgram.get(i).getSeller());
			}
	}

}
