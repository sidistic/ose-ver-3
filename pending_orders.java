package stock_exchange_2;

import java.util.ArrayList;

public class pending_orders {
	ArrayList<Order> orders = new ArrayList<Order>();
//	Order new_order;
	public void add_new_order(Order new_order)
	{
		orders.add(new_order);
	}
	
	public void update_orders(ArrayList<Order> updated_orders)
	{
		for(int i=0;i<updated_orders.size();i++)
		{
			if(updated_orders.get(i).getQuantity()==0)
			{
				updated_orders.remove(i);
				i--;
			}
		}
		orders = updated_orders;
	}
	
	public void print_pending()
	{
		System.out.println("Time\tType\tFrom\tStock\tQnt\tPrice");
		for(int i=0;i<orders.size();i++)
		{
			Order temp = orders.get(i);
			System.out.println(temp.time +"\t" + temp.type
					+ "\t" + temp.client + "\t" + temp.stock + "\t"
					+ temp.quantity + "\t" + temp.price);
		}
	}
}
