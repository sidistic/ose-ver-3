package stock_exchange_2;
//import java.io.*;
//import java.util.*;
public class Order {
	// class which defines the structure for an order
	float price; // price at which the client wants to sell/buy the stock 
	int time; // time of arrival of order
	String stock; // The name of stock
	String client; // The name of client
	float quantity; // The quantity client want to sell or buy
	String type; // Whether he's a seller or a buyer
	public void setPrice(float price) {
		this.price = price;
	}
	
	public void setTime(int time) {
		this.time = time;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	
	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public void setType(String type) {
		this.type = type;
	}
	public float getPrice() {
		return price;
	}
	
	public int getTime() {
		return time;
	}
	public String getType() {
		return type;
	}
	public String getStock() {
		return stock;
	}
	
	public float getQuantity() {
		return quantity;
	}
	public String getClient() {
		return client;
	}
}
