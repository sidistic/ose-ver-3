package stock_exchange_2;

import java.io.*;
import java.util.*;

public class Main {
	public static void main(String[] args) throws FileNotFoundException {
		//String fileName= args[0];
		//Use the above to give filename as arguments and change the path to readFile(fileName) below. 
		pending_orders pending = new pending_orders();
		Order_match match = new Order_match();
		Register data = new Register();
		
		Scanner inputStream;
		File file= new File("/home/siddharth/Desktop/Stock.csv"); // read into a file
	    inputStream = new Scanner(file);
		while(inputStream.hasNext())
		{
			String line= inputStream.next(); // line contains all the csv data
//			System.out.println(line);
			String[] values = line.split(",");
			
			data.register_new_stock(values[0], Float.valueOf(values[1]));
		}
		inputStream.close();
		
		
		
		file= new File("/home/siddharth/Desktop/Customer.csv"); // read into a file
	    inputStream = new Scanner(file);
		while(inputStream.hasNext())
		{
			String line= inputStream.next(); // line contains all the csv data
//			System.out.println(line);
			String[] values = line.split(",");
			data.register_new_customer(values[0], Float.valueOf(values[1]));
		}
		inputStream.close();
		
		
	    file= new File("/home/siddharth/Desktop/ticker.csv"); // read into a file
	    inputStream = new Scanner(file);
		while(inputStream.hasNext())
		{
			String line= inputStream.next(); // line contains all the csv data
//			System.out.println(line);
			String[] values = line.split(",");
			Order order = new Order();
			order.setTime(Integer.parseInt(values[0])); //sets the respective fields for an order
			order.setType(values[1]);
			order.setClient(values[2]);
			order.setStock(values[3]);
			order.setQuantity(Float.valueOf((values[4])));
			order.setPrice(Float.valueOf((values[5])));
			
			/*This part checks if the new order satisfies the following conditions:
			 * 1. The client is a registered customer
			 * 2. The stock is registered
			 * 3. The price of the stock is +/- 10% of the price registered*/
			if(data.check_customer(order.client)!=-1)
			{
				int index_stock = data.check_stock(order.stock);
				if(index_stock!=-1)
				{
					if(order.price<1.1*data.registered_stocks.get(index_stock).price && order.price>0.9*data.registered_stocks.get(index_stock).price)
					{
						pending.add_new_order(order);
						pending.update_orders(match.findMatch(pending.orders,data));
					}
				}
				
			}
			
		}
//		System.out.println("Pending Orders");
//		pending.print_pending();
//		System.out.println("\nTrades made");
//		match.print_trades();
//		System.out.println("\nCustomer Details");
//		data.print_customers();
		
		
		match.print_customerwise("C2");

		inputStream.close();
	}
}
